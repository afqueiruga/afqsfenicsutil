# afq_fenicsutil

Alejandro Franisco Queiruga  
B. Emek Abali  
2017-2018

## Intro 

A little collections of utilities for FEniCS. Portions of it are taken from
various codes by afq and  B.E. Abali. We use it for many different
applications. Most notably, the algorithm in
[mesh_morph.py](mesh_morph.py) is a crucial component to an upcoming
paper:

> TODO Cite the paper here

## Style guide

Following Python practices, please use four spaces per indent level. Tabs should appear NOWHERE in the source. (I'm talking to you, Emek!)
Emek loves TABS!

## License

Copyright (C) Alejandro Francisco Queiruga and B. Emek Abali, 2017-2018

 is released under version 3 of the GNU Lesser General Public License, as per LICENSE.txt.

Please mention this repository if you use this library.
